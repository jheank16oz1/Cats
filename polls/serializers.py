from django.conf.urls import url, include
from .models import Question,Cat
from rest_framework import routers, serializers, viewsets

# Serializers define the API representation.
class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question
        fields = ('question_text', 'pub_date')

class CatSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cat
        fields = ('name', 'description', 'image')